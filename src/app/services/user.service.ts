import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';

import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private url = `http://localhost:8000/api/user/`;
  private headers = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
    body: null,
  };

  constructor(private http: HttpClient) {}

  getAll(): Observable<User[]> {
    return this.http.get<User[]>(this.url, this.headers);
  }
}
